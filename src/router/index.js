import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Testi from '@/components/Testi'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: HelloWorld,
      mode: 'history'
    },
    {
      path: '/testi',
      name: 'Testi',
      component: Testi,
      mode: 'history'
    }
  ]
})
